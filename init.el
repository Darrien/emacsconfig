;; https://gitlab.com/taurhine/tauremacs

;;; to install dependencies of this config on a debian like distro run
;;; apt install global flake8 clang silversearcher-ag

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(add-to-list 'load-path "~/.emacs.d/custom")
(require 'setup-internal)
(require 'setup-external)
(require 'setup-bindings)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ~~ Language Support ~~
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;C/C++
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Syntax Chk:     Flycheck-clang
;;Autocompletion: Company-Irony / clang
;;Debugging:      realgud
;;Refactoring:    -
;;Tagging:        Gnu Global
;;
;;Ext. Dependencies: clang, global
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Python
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Syntax Chk:     Flycheck with flake8
;;Autocompletion: Company-Jedi, Company-Quickhelp for popup docstring
;;Debugging:      pdb
;;Refactoring:    -
;;Ext. Dependencies: flake8, rest of the dependencies will be installed by jedi:install-server command
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;E-Lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Syntax Chk:     Flycheck
;;Autocompletion: Company-Elisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-scrollbar-bg ((t (:background "#3d3d3d"))))
 '(company-scrollbar-fg ((t (:background "#303030"))))
 '(company-tooltip ((t (:inherit default :background "#3d3d3d"))))
 '(company-tooltip-annotation ((t (:inherit font-lock-function-name-face))))
 '(company-tooltip-common ((t (:inherit font-lock-constant-face))))
 '(company-tooltip-selection ((t (:inherit font-lock-function-name-face))))
 '(mode-line ((t (:background "black" :foreground "#f6f3e8")))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-support-shift-select t)
 '(package-selected-packages
   (quote
    (debbugs php-mode zygospore volatile-highlights visual-regexp-steroids use-package undo-tree todotxt realgud rainbow-delimiters openwith magit iedit ido-completing-read+ helm-projectile helm-gtags helm-ag flycheck-irony flycheck-clang-analyzer elfeed-goodies company-quickhelp company-jedi company-irony-c-headers company-irony company-c-headers comment-dwim-2 cmake-mode buffer-move anzu android-mode ag)))
 '(python-shell-interpreter "python3"))

;;; init.el ends here
