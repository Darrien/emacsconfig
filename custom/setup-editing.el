;; https://gitlab.com/taurhine/tauremacs

;;make tabs visible with a special character in all modes
(require 'whitespace)
(setq whitespace-style '(trailing tabs tab-mark trailing-mark))
;; show unncessary whitespace that can mess up your diff
(add-hook 'prog-mode-hook
          (lambda () (interactive)
            (whitespace-mode)
            (setq show-trailing-whitespace 1)
            ))

;; show whitespace in diff-mode
(add-hook 'diff-mode-hook (lambda ()
                            (setq-local whitespace-style
                                        '(face
                                          tabs
                                          tab-mark
                                          spaces
                                          space-mark
                                          trailing
                                          indentation::space
                                          indentation::tab
                                          newline
                                          newline-mark))
                            (whitespace-mode 1)))

(use-package rainbow-delimiters
  :ensure t
  :config
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

(use-package volatile-highlights
  :init
  (volatile-highlights-mode t))

(use-package undo-tree
  :init
  (global-undo-tree-mode 1))

(use-package anzu
  :init
  (global-anzu-mode))

(use-package iedit
  :init
  (setq iedit-toggle-key-default nil))

(use-package comment-dwim-2)

;; kill a line, including whitespace characters until next non-whitepsace character of next line
(defadvice kill-line (before check-position activate)
  (if (member major-mode
              '(emacs-lisp-mode scheme-mode lisp-mode
                                c-mode c++-mode objc-mode
                                latex-mode plain-tex-mode))
      (if (and (eolp) (not (bolp)))
          (progn (forward-char 1)
                 (just-one-space 0)
                 (backward-char 1)))))

;; taken from prelude-editor.el
;; automatically indenting yanked text if in programming-modes
(defvar yank-indent-modes
  '(LaTeX-mode TeX-mode)
  "Modes in which to indent regions that are yanked (or yank-popped).
Only modes that don't derive from `prog-mode' should be listed here.")

(defvar yank-indent-blacklisted-modes
  '(python-mode slim-mode haml-mode)
  "Modes for which auto-indenting is suppressed.")

(defvar yank-advised-indent-threshold 1000
  "Threshold (# chars) over which indentation does not automatically occur.")

(defun yank-advised-indent-function (beg end)
  "Do indentation, as long as the region isn't too large."
  (if (<= (- end beg) yank-advised-indent-threshold)
      (indent-region beg end nil)))

(defadvice yank (after yank-indent activate)
  "If current mode is one of 'yank-indent-modes, indent yanked text (with prefix arg don't indent)."
  (if (and (not (ad-get-arg 0))
           (not (member major-mode yank-indent-blacklisted-modes))
           (or (derived-mode-p 'prog-mode)
               (member major-mode yank-indent-modes)))
      (let ((transient-mark-mode nil))
        (yank-advised-indent-function (region-beginning) (region-end)))))

(defadvice yank-pop (after yank-pop-indent activate)
  "If current mode is one of `yank-indent-modes', indent yanked text (with prefix arg don't indent)."
  (when (and (not (ad-get-arg 0))
             (not (member major-mode yank-indent-blacklisted-modes))
             (or (derived-mode-p 'prog-mode)
                 (member major-mode yank-indent-modes)))
    (let ((transient-mark-mode nil))
      (yank-advised-indent-function (region-beginning) (region-end)))))

;;tidy up the code before saving
(defun tp-code-tidy-up()
  (if (not (equal major-mode 'makefile-gmake-mode))
      (untabify (point-min) (point-max));;don't untabify in makefiles
  )
  (delete-trailing-whitespace)
)

(add-hook 'prog-mode-hook
           (lambda () (add-hook 'before-save-hook 'tp-code-tidy-up nil 'local)))

;;autoguess indentation style
(add-hook 'c-mode-common-hook (lambda ()
                                (setq indent-tabs-mode nil)
                                (c-set-offset 'substatement-open 0)
                                (setq c-basic-offset 3) ;;temporary solution is to set the basic offset to 3
                                (setq c-indent-level 0)))

;;autoguess indentation style (does not update the basic offset works only for the next lines)
(defun smie-auto-guess ()
  (when (featurep 'smie)
    (unless (eq smie-grammar 'unset)
      (smie-config-guess))))

(add-hook 'after-change-major-mode-hook 'smie-auto-guess)

(provide 'setup-editing)

;;; setup-editing.el ends here
