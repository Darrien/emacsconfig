;; https://gitlab.com/taurhine/tauremacs

;; setup company

(use-package company
  :init
  (global-company-mode 1)
  (delete 'company-semantic company-backends)
  )

(use-package company-quickhelp
  :init
  (company-quickhelp-mode 1)
  )

(use-package company-c-headers
  :init
  (add-to-list 'company-backends 'company-c-headers))

;; setup irony mode
;; irony installation requires user to run irony-install-server once after installation!!
(use-package irony)
(use-package company-irony)
(use-package company-irony-c-headers)

(add-hook 'c-mode-common-hook 'irony-mode)

;;(add-hook 'c++-mode-hook 'irony-mode)
;;(add-hook 'c-mode-hook 'irony-mode)
;;(add-hook 'objc-mode-hook 'irony-mode)

(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

(eval-after-load 'company
     '(add-to-list 'company-backends 'company-irony)
;;   '(add-to-list 'company-backends 'company-semantic)
)

(add-to-list 'company-backends 'company-capf)


;;Enabling jedi mode (autocompletion) for python
(use-package company-jedi)

(add-hook 'python-mode-hook 'jedi:setup)
(setq jedi:complete-on-dot t)

(defun my/python-mode-hook ()
  (add-to-list 'company-backends 'company-jedi))

(add-hook 'python-mode-hook 'my/python-mode-hook)

(provide 'setup-company)
