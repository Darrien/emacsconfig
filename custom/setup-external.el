;; https://gitlab.com/taurhine/tauremacs

;; set the flag to initialise internal setup key-bindings later on
(setq internal-bindings nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Melpa and Use Package ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'package)

(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)

(when (not package-archive-contents)
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;
;; Project Management ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package projectile
  :init
  (projectile-mode)
  (setq projectile-enable-caching t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Buffer / Window Management ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package zygospore)
(use-package buffer-move)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Organisation / Documentation ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package org)

;; enable support for ditaa
(org-babel-do-load-languages 'org-babel-load-languages '(
                                                         (ditaa . t)
                                                         (plantuml . t)
                                                         )
                             )
(setq org-plantuml-jar-path (expand-file-name "~/.plantuml/plantuml201801.jar"))
(setq org-ditaa-jar-path (expand-file-name "/usr/bin/ditaa"))

(add-hook 'org-babel-after-execute-hook #'org-redisplay-inline-images)

;; #+BIND allows variables to be set on export without confirmation.
(setq org-export-allow-BIND t)

(push '("\\.org\\'" . org-mode) auto-mode-alist)
(push '("\\.org\\.txt\\'" . org-mode) auto-mode-alist)

(use-package plantuml-mode)
;; Enable plantuml-mode for PlantUML files
(add-to-list 'auto-mode-alist '("\\.uml\\'" . plantuml-mode))

(use-package todotxt)
(add-to-list 'auto-mode-alist '("\\todo.txt\\'" . todotxt-mode))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;
;; Regex ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Package visual-regexp and visual-regexp-steroids for building regexp with python dialect
(use-package visual-regexp)
(use-package visual-regexp-steroids)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;
;; Syntax Checking ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))
(add-hook 'after-init-hook #'global-flycheck-mode)
(setq flycheck-check-syntax-automatically '(mode-enabled save))

;; Setup clang analyzer
(use-package flycheck-clang-analyzer)

(with-eval-after-load 'flycheck
  (require 'flycheck-clang-analyzer)
  (flycheck-clang-analyzer-setup))

(use-package flycheck-irony)
(eval-after-load 'flycheck
  '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;
;; Searching ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package ag)
(use-package helm-ag)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;
;; Make / Build ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package cmake-mode)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;
;; Debugging ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package realgud)

;;Compilation and Debugging Settings
;;scroll to the first error in the compilation buffer
(setq compilation-scroll-output 'first-error)

;; setup GDB
(setq
 ;; Non-nil value will display source file containing the main routine at startup
 gdb-show-main t)

(defun function-with-refresh (function)
  "Run `a function' and `gud-refresh' in sequence."
  (funcall function nil)
  (sit-for 0.1)
  (gud-refresh nil))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'setup-company)
(require 'setup-helm)
(require 'setup-editing)
(require 'setup-magit)

;; Following lines change the colour of the mode-line depending on whether the window is dedicated or not
;; although it is a block of code which is supposed to be the part of the setup-internal, calling it before
;; setup-helm seems to break something
(lexical-let ((default-color (cons "black" "#f6f3e8")))
  (add-hook 'post-command-hook
            (lambda ()
              (let ((color (cond ((minibufferp) default-color)
                                 ((window-dedicated-p) '("#000040" . "#ffffff"))
                                 (t default-color))))
                (set-face-background 'mode-line (car color))
                (set-face-foreground 'mode-line (cdr color)))))
)

(provide 'setup-external)
